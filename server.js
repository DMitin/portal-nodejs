const express = require('express');
const bodyParser = require('body-parser');
var exitHook = require('exit-hook');
/*
var morgan = require('morgan');
var morganBody = require('morgan-body');
*/

var logger = require('./config/logger');
const auth = require('./src/auth');
const routes = require('./src/routes');

const config = require('./config/config')();



const app = express();
const port = config.port;
console.log(port);

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

/*morganBody(app);
app.use(morgan(":method :url :status", {
  stream: {
    write: (message) => {
      logger.info(message);
    }
  }
}));
*/
app.use(express.static("public"));
routes(app);
app.listen(port, () => {
  console.log('We are live on ' + port);
});

exitHook(function () {
  console.log('clean application data');
  driver.close();
});

module.exports = app;

