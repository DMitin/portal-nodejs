const env = process.env.NODE_ENV;
let ldap;
if (env) {
  ldap = require(`./ldap.${env}.js`);
} else {
  ldap = require("./ldap.dev.js");
}

module.exports = ldap;