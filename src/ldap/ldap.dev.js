var request = require('request');


module.exports = {
  auth: (ldapName, password) => {
    return new Promise((resolve, reject) => {
      request.post("http://localhost:8080/ldap",{
        json: {
          ldapName,
          password
        }
      }, (err, resp, body) => {
        if (!err && resp.statusCode == 200) {
          resolve(body.status);
        } else {
          resolve(false);
        }
      });
    });
  }
};