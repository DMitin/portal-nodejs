const users = [{
  ldapName: "dMitin",
  password: "123"
}, {
  ldapName: "aEgorov",
  password: "123"
}, {
  ldapName: "ePirogov",
  password: "123"
}];

module.exports = {
  auth: (ldapName, password) => {
    return new Promise((resolve, reject) => {
      resolve(users
        .filter(u => u.ldapName === ldapName && u.password === password)
        .length > 0);
    });
  },

  validUsers: users,
  notValidUser: {
    ldapName: "xXxxxxx",
    password: "xxx"
  }
};