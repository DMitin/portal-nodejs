var formidable = require('formidable');

var path = require('../../config/path');


module.exports = function (app, session) {

  app.post('/upload', function (req, res) {
    var form = new formidable.IncomingForm();
    form.uploadDir = path.imagesFolderPath;
    form.parse(req);

    var fileName;
    form.on('fileBegin', function (name, file) {
      fileName = file.name;
      file.path = form.uploadDir + "/" + fileName;
    });

    form.on('error', function (err) {
      res.end(err);
    });

    form.on('end', function () {
      res.end(fileName);
    });
  });
};