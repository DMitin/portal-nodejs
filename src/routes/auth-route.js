var jwt = require("jwt-simple");
const neo4jAuth = require('../business/neo4j-authUsert');
const cfg = require('../../config/auth.config');
const ldap = require('../ldap/ldap');

module.exports = function (app) {

  app.post('/token', (req, res) => {

    //implement LDAP search
    if (req.body.ldapName && req.body.password) {
      const ldapName = req.body.ldapName;
      const password = req.body.password;
      ldapPromise = ldap.auth(ldapName, password)
        .then(ldapResult => {
          if (ldapResult) {
            neo4jAuth.searchUserIdByLdapName(ldapName)
              .then(userId => { //returns user id
                if (userId) {
                  var token = jwt.encode({
                    id: userId
                  }, cfg.jwtSecret);
                  res.send({
                    token: token
                  });
                } else {
                  res
                    .status(401)
                    .send("No account in neo4j");
                }
              })
              .catch(err => {
                res
                  .status(500)
                  .send("ERROR")
              });
          } else {
            res
              .status(401)
              .send("LDAP auth error");
          }
        });
    } else {
      res
        .status(401)
        .send("please provide valida credentials for user {ldapName, password}");
    }
  });
};