module.exports = () => {

  const mapUser = (node) => {
    delete node.properties.searchString;
    return node;
  };
  const mapNode = (node) => {
    if (node.labels.indexOf("User") > -1) {
      return mapUser(node);
    } else {
      return node;
    }
  };

  /**
   * Format single
   * @param item
   */
  const mapShortNode = (item) => {
    return mapNode({
      id: item._fields[0].identity.toString(),
      labels: item._fields[0].labels,
      properties: item._fields[0].properties
    })
  };

  return {
    mapUser,
    mapNode,
    mapShortNode
  }
};