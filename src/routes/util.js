module.exports = {
  isAdmin: (roles) => roles.indexOf("Admin") > -1,
  addSearchStringToUserData: (userData) => {
    const searchString =
      userData.firstName.toLowerCase() + " " + userData.lastName.toLowerCase();

    return Object.assign({}, userData, {searchString});
  }
};