const neo4jObject = require('../business/neo4j-object')();
const util = require('./util');

const labelName = "Role";

module.exports = function (app) {

  app.get('/roles', (req, res) => {
    neo4jObject.getAll(labelName)
      .then(result => {
        res.send(result.records.map(item => item._fields));
      }).catch(function (error) {
      res
        .status(500)
        .send(error);
    });
  });


  app.post('/roles', (req, res) => {

    if (!util.isAdmin(req.user.roles)) {
      res
        .status(401)
        .send("Only Admins can crate new users");
      return;
    }

    const roleData = {
      name: req.body.name
    };

    if (!(roleData.name)) {
      res
        .status(406)
        .send("To create user, please specify: {name}");
      return;
    }

    neo4jObject.createObject(labelName, roleData)
      .then(result => res.send(result))
      .catch(function (error) {
        res
          .status(500)
          .send(error);
      });
  });
};