const userRoutes = require('./user_routes');
const roleRoutes = require('./role_routes');
const relRoutes = require('./relationship_router');
const skillRoutes = require('./skill_routes');
const uploader = require('./uploader');
const authRouter = require('./auth-route');
const authProvider = require("../auth.js")();
module.exports = function(app) {

  authRouter(app);
  app.use(authProvider.authenticate());
  userRoutes(app);
  roleRoutes(app);
  uploader(app);
  relRoutes(app);
  skillRoutes(app);

};