const fs = require('fs-extra');
const neo4jObject = require('../business/neo4j-object')();
const path = require('../../config/path');
const util = require('./util');
const {mapUser, mapNode, mapShortNode} = require('./mapper')();

const FILE_ERROR_CONST = "FILE_ERROR!";

module.exports = function (app) {

  app.get('/users', (req, res) => {
    neo4jObject.getAll("User")
      .then(result => {
        res.send(result.records.map(item => mapShortNode(item)))
      }).catch(function (error) {
      res
        .status(500)
        .send(error);
    });
  });

  app.get('/users/:id', (req, res) => {
    const id = req.params.id;
    neo4jObject.getOne(id)
      .then(result => {
        const rawNode = result
          .records[0]
          ._fields[0];
        const node = mapUser({
          id: rawNode.identity.toString(),
          labels: rawNode.labels,
          properties: rawNode.properties
        });
        node.relations = result
          .records[0]
          ._fields[1]
          .filter(e => e.rel !== null)
          .map(r => {
            return {
              relInfo: {
                id: r.rel.identity.toString(),
                type: r.rel.type,
                properties: r.rel.properties
              }, node: mapNode({
                id: r.end.identity.toString(),
                labels: r.end.labels,
                properties: r.end.properties
              })
            }
          });
        res.send(node);
      }).catch(function (error) {
      res
        .status(500)
        .send(error);
    });
  });


  app.post('/users', (req, res) => {

    if (!util.isAdmin(req.user.roles)) {
      res
        .status(401)
        .send("Only Admins can crate new users");
      return;
    }

    const userData = util.addSearchStringToUserData({
      firstName: req.body.firstName,
      lastName: req.body.lastName,
      image: req.body.image,
      ldapName: req.body.ldapName,
    });

    if (!(userData.firstName && userData.lastName
        && userData.image && userData.ldapName)) {
      res
        .status(406)
        .send("To create user, please specify: {firstName, lastName, image, ldapName}");
      return;
    }

    const imagePath = path.imagesFolderPath + "/" + userData.image;
    fs.stat(imagePath)
      .catch(() => {throw FILE_ERROR_CONST}) //original err in catch is lost
      .then(() => neo4jObject.createObject("User", userData))
      .then(result => {
        res.send(mapUser(result))
      })
      .catch(function (err) {
        if (err === FILE_ERROR_CONST) {
          res
            .status(406)
            .send("Please upload user image: " + userData.image);
        } else {
          res
            .status(500)
            .send(err);
        }
      });
  });

    app.post('/users/search/:searchStr', (req, res) => {
        const searchString = req.params.searchStr;
        neo4jObject.search("User", "searchString", searchString)
        .then(result => {
          res.send(result.records.map(item => {
              return mapNode({
               id: item._fields[0].identity.toString(),
               labels: item._fields[0].labels,
               properties: item._fields[0].properties
              })
            }))
        }).catch(function (error) {
            res
            .status(500)
            .send(error);
        });
    });
};