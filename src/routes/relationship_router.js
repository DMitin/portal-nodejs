const {toString} = require('neo4j-driver').v1.integer;
const neo4jRelationship = require('../business/neo4j-relationship')();
const neo4jLabel = require('../business/neo4j-checkLabels');
const labels = require('../business/labels');
const util = require('./util');

const LABELS_ERROR = "LABELS_ERROR";

const isConnectionPermitted = (role1, role2) => {
  return (role1 === labels.USER_LABEL) && (role2 === labels.ROLE_LABEL) ||
    (role1 === labels.USER_LABEL) && (role2 === labels.SKILL_LABEL)
};

module.exports = function (app) {

  app.post('/relationship', (req, res) => {

    if (!util.isAdmin(req.user.roles)) {
      res
        .status(401)
        .send("Only Admins can crate new users");
      return;
    }

    const relData = {
      from: req.body.from,
      to: req.body.to
    };

    if (!(relData.from && relData.to)) {
      res
        .status(406)
        .send("Please specify {from, to} values");
      return;
    }


    neo4jLabel.getLabels(relData.from, relData.to)
      .then((map) => {
        if (!isConnectionPermitted(map["l1"], map["l2"])) {
          throw LABELS_ERROR;
        }
      })
      .then(() => neo4jRelationship.createRelationship(relData.from, relData.to, "HAS_ROLE"))
      .then(result => {
        const relData = result.records[0]._fields[0];
        res.send({
          id: relData.identity.toString(),
          start: relData.start.toString(),
          end: relData.end.toString(),
          type: relData.type,
          properties: relData.properties
        });
      })
      .catch(function (err) {
        if (err === LABELS_ERROR) {
          res
            .status(406)
            .send("from id should be User, to id should be Role");
        } else {
          res
            .status(500)
            .send(err);
        }
      });


  });
};