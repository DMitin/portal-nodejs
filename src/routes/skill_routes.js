const neo4jObject = require('../business/neo4j-object')();
const {mapShortNode} = require('./mapper')();

module.exports = function (app) {
  app.get('/skills', (req, res) => {
    neo4jObject.getAll("Skill")
      .then(result => {
        res.send(result.records.map(item => mapShortNode(item)))
      }).catch(function (error) {
        res
          .status(500)
          .send(error);
        });
    });
}