const {int} = require('neo4j-driver').v1.integer;
const {toString} = require('neo4j-driver').v1.integer;
const session = require('./neo4j');
const {closable} = require('./util');


module.exports = function () {
  return {
    createObject: (label, properties) => {
      return closable(session.run(
        `CREATE (a:${label} {properties}) RETURN a`,
        {properties}
      )).then(res => {
        const singleRecord = res.records[0];
        const node = singleRecord.get(0);
        return {
          id: node.identity.toString(),
          labels: node.labels,
          properties: node.properties
        }
      });
    },
    getAll: (label) => {
      return closable(session.run(
        `MATCH (u:${label}) RETURN u`
      ));
    },
    getOne: (idString) => {
      return closable(session.run(
        `START n=node(${idString}) ` +
        "OPTIONAL MATCH (n)-[r]-(e) " +
        "return n, collect({rel: r, end:  e})"
      ));
    },

    search: (label, searchField, searchText) => {
      return closable(session.run(
        `MATCH (e:${label}) ` +
        `WHERE e.${searchField} CONTAINS {searchText} ` +
        "RETURN e " +
        "ORDER BY e.lastName",
        {searchText}
      ));
    }
  }
};