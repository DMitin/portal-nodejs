const session = require('./neo4j');
const {closable} = require('./util');

module.exports = function () {
  return {
    createRelationship: (idFrom, idTo, relType) => {
      return closable(session.run(
        `START from=node(${idFrom}), to=node(${idTo})` +
        `CREATE (from)-[r:${relType}]->(to) RETURN r`
      ));
    }
  }
};