const session = require('./neo4j');
const {closable} = require('./util');

module.exports = {
  getLabels: (id1, id2) => {
    return closable(Promise.all([
      session.run(`START n1=node(${id1}) return labels(n1) as l1`),
      session.run(`START n2=node(${id2}) return labels(n2) as l2`),
    ]))
      .then((res) => {
        const map = res.reduce((b, e) => {
          const key = e.records[0]
            .keys[0];
          b[key] = e.records[0]
            ._fields[0][0];
          return b;
        }, {});
        return map;
      })

  }
};