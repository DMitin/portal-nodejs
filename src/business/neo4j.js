const neo4j = require('neo4j-driver').v1;

const driver = neo4j.driver("bolt://127.0.0.1:7687", neo4j.auth.basic("neo4j", "secr8t"));
const session = driver.session();

module.exports = session;