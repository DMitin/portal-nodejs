const session = require('./neo4j');

module.exports = {
  closable: p => {
    p.then(result => {
      session.close();
      return result;
    }).catch(error => {
      session.close();
      throw error;
    });
    return p;
  }
};