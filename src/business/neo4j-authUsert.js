const session = require('./neo4j');
const {closable} = require('./util');

module.exports = {

  searchUserIdByLdapName: (ldapName) => {
    return closable(session.run(
      "OPTIONAL MATCH (n {ldapName: {ldapName}})\n" +
      "return id(n)",
      {ldapName}
    ))
      .then(res => {
        const nodeBase = res
          .records[0]
          ._fields[0];
        if (nodeBase) {
          return nodeBase.toString(); // we return pure id as string
        } else {
          return null; // or null, if no user found
        }
      });
  },

  /* COOL REQUEST )))
  "OPTIONAL MATCH (n {ldapName: {ldapName}}) " +
  "OPTIONAL MATCH (n)-[:HAS_ROLE]-(r) " +
  "return n, collect(r.name) ",
*/
  getUserWithRolesById: (id) => {
    return closable(session.run(
      `START n=node(${id}) ` +
      "OPTIONAL MATCH (n)-[:HAS_ROLE]-(r) " +
      "return collect(r.name) "
    ))
      .then(res => {
        const resultBase = res
          .records[0]
          ._fields[0];
        if (resultBase) {
          return resultBase;
        } else {
          return [];
        }
      })
  }
};