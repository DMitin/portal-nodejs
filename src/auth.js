const passport = require('passport');
const {Strategy, ExtractJwt} = require('passport-jwt');
const neo4jAuth = require('./business/neo4j-authUsert');
const authConfig = require('../config/auth.config');

module.exports = () => {

  const params = {
    secretOrKey: authConfig.jwtSecret,
    jwtFromRequest: ExtractJwt.fromAuthHeader()
  };
  const strategy = new Strategy(params, (payload, done) => {
    const id = payload.id;
    neo4jAuth.getUserWithRolesById(id)
      .then(res => {
        return done(null, {
          id: id,
          roles: res
        });
      })
      .catch(err => {
        return done(err, null);
      });
  });
  passport.use(strategy);
  return {
    initialize: () => {
      return passport.initialize();
    },
    authenticate: () => {
      return passport.authenticate("jwt", authConfig.jwtSession);
    }
  };
};