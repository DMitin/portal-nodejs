const appRoot = require('app-root-path');
const path = require('path');

const imagesLocalPath = '/public/images';


module.exports = {
  imagesFolderPath: path.join(appRoot.path, imagesLocalPath)
};