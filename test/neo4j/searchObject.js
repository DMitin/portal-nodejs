const neo4jInit = require('../util/initNeo4j');
const crud = require('../util/crud')();

const neo4jObject = require('../../src/business/neo4j-object')();

describe("Neo4j Internal services (not public API). Searching", () => {

  let user, role;

  before(done => {
    neo4jInit.initNeo4jPromise()
      .then(() => crud.createCommonUser())
      .then(() => done())
      .catch(err => done(err));
  });

  /*
  it("search common user", done => {
    neo4jObject.search("User", "searchString", "mit")
      .then(res => {
        const nodeBase = res
            .records[0]
            ._fields[0];
        expect(nodeBase.id).a("string");
        expect(nodeBase.properties).to.eql(crud.commonUserData);
        done();
      })
      .catch(err => done(err));
  });
*/
});