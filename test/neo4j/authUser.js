const neo4jInit = require('../util/initNeo4j');
const crud = require('../util/crud')();
const neo4jAuth = require('../../src/business/neo4j-authUsert');

const strangeLdapName = "otherLdapName";

describe("Neo4j services. Search Ldap Users in neo4j", () => {

  let noRolesUser, userWithRoles;
  let role1, role2;

  const specialUserData = Object.assign({}, crud.commonUserData, {
    ldapName: strangeLdapName
  });

  before(done => {

    neo4jInit.initNeo4jPromise()
      .then(() => crud.createCommonUser())
      .then((id) => noRolesUser = id)
      .then(() => crud.createCommonUser(specialUserData))
      .then((id) => userWithRoles = id)
      .then(() => crud.createCommonRole("RoleTest1"))
      .then((id) => role1 = id)
      .then(() => crud.createCommonRole("RoleTest2"))
      .then((id) => role2 = id)
      .then(() => crud.createRelationship(userWithRoles, role1, "HAS_ROLE"))
      .then(() => crud.createRelationship(userWithRoles, role2, "HAS_ROLE"))
      .then(() => done())
      .catch(err => done(err));

  });

  it("Search user with no roles, by ldapName", done => {
    neo4jAuth.searchUserIdByLdapName(crud.commonUserData.ldapName)
      .then(res => {
        expect(res).to.be.a('string'); //check id
        //expect(res.roles).to.be.an('array').that.is.empty;
        done();
      })
      .catch(err => done(err));
  });

  it("Search user with fake (not existing) ldapName", done => {
    neo4jAuth.searchUserIdByLdapName("fale_ldap_name")
      .then(res => {
        expect(res).to.be.null;
        done();
      })
      .catch(err => done(err));
  });

  it("Get user roles by id", done => {
    neo4jAuth.getUserWithRolesById(userWithRoles)
      .then(res => {
        expect(res)
          .to.be.an('array')
          .that.have.lengthOf(2)
          .that.includes('RoleTest1', 'RoleTest2');
        done();
      })
      .catch(err => done(err));
  });

  it("Get empty user roles by id", done => {
    neo4jAuth.getUserWithRolesById(noRolesUser)
      .then(res => {
        expect(res)
          .to.be.an('array')
          .that.is.empty;
        done();
      })
      .catch(err => done(err));
  });

  it("Get roles with invalid id as string returns null", done => {
    neo4jAuth.getUserWithRolesById("zzz") // string instead of number in string form
      .then(res => {
        done(new Error("should return error"));
      })
      .catch(err => {
        expect(err.code).to.be.equal("Neo.ClientError.Statement.SyntaxError");
        done()
      });
  });

  it("Get roles with not existing id returns error", done => {
    const notExistingId = userWithRoles + "0";
    neo4jAuth.getUserWithRolesById(notExistingId)
      .then(res => {
        done(new Error("should return error"));
      })
      .catch(err => {
        expect(err.code).to.be.equal("Neo.ClientError.Statement.EntityNotFound");
        done()
      });
  });
});