const neo4jInit = require('../util/initNeo4j');
const crud = require('../util/crud')();

const neo4jLabels = require('../../src/business/neo4j-checkLabels');

describe("Neo4j Internal services (not public API). Getting labels by id", () => {

  let user, role;

  before(done => {
    neo4jInit.initNeo4jPromise()
      .then(() => crud.createCommonUser())
      .then((id) => user = id)
      .then(() => crud.createCommonRole("RoleTest1"))
      .then((id) => role = id)
      .then(() => done())
      .catch(err => done(err));
  });

  it("get disconnectedUser (with no relations)", done => {
    neo4jLabels.getLabels(user, role)
      .then(map => {

        expect(map["l1"]).to.eql("User");
        expect(map["l2"]).to.eql("Role");

        done();
      })
      .catch(err => done(err));
  });
});