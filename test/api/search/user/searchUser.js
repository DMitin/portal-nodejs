const neo4jInit = require('../../../util/initNeo4j');
const download = require('../../../util/downloadFiles');
const crud = require('../../../util/crud')();


const foxFile = "fox.png";

describe("Search Users", () => {

    var adminUser;
    var adminRole;
    var token;

    before(done => {
        neo4jInit.initNeo4jPromise()
            .then(() => download.removeDownloadedFile(foxFile))
            .then(() => crud.createCommonUserCredentials())
            .then((res) => {
                adminUser = res.userId;
                token = res.token
            })
            .then(() => crud.createCommonRole("Admin"))
            .then((id) => adminRole = id)
            .then(() => crud.createRelationship(adminUser, adminRole, "HAS_ROLE"))
            .then(() => done())
            .catch(err => done(err));
    });

    it("can search user", done => {
        request.post("/users/search/mit")
            .set("Authorization", `JWT ${token}`)
            .end((err, res) => {
                expect(res.body[0].id).a("string");
                expect(res.body[0].properties).to.eql(crud.commonUserData);
                done(err)
            });
    });
});