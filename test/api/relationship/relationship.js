const crud = require('../../util/crud')();
const neo4jInit = require('../../util/initNeo4j');
const neo4jObject = require('../../../src/business/neo4j-object')();


describe("Relationship API", () => {

  let adminUserId;
  let adminToken;
  let roleId;
  let skillId;
  const skillName = "Java";


  before(done => {
    neo4jInit.initNeo4jPromise()
      .then(() => crud.createAdminUserCredentials())
      .then(({userId, token}) => {
        adminUserId = userId;
        adminToken = token;
      })
      .then(() => crud.createCommonRole())
      .then((id) => roleId = id)
      .then(() => neo4jObject.createObject("Skill", {
        name: skillName
      }))
      .then((res) => skillId = res.id)
      .then(() => done())
      .catch(err => done(err));
  });

  it("can't create relationship, if don't specify {from,to}", done => {
    request.post("/relationship")
      .set("Authorization", `JWT ${adminToken}`)
      .send({from: adminUserId})
      .expect(406)
      .then(() => done())
      .catch(err => done(err));
  });

  it("can't create relationship, with erong types (two nodes are users)", done => {
    request.post("/relationship")
      .set("Authorization", `JWT ${adminToken}`)
      .send({ from: adminUserId, to: adminUserId}) //two userId parameters!
      .expect(406)
      .then(() => done())
      .catch(err => done(err));
  });

  it("create relationship between User and Role", done => {
    request.post("/relationship")
      .set("Authorization", `JWT ${adminToken}`)
      .send({from: adminUserId, to: roleId})
      .expect(200)
      .then((res) => {
        expect(res.body.id).a("string")
        expect(res.body.start).to.eql(adminUserId)
        expect(res.body.end).to.eql(roleId)
        expect(res.body.type).to.eql("HAS_ROLE")
        expect(res.body.properties).to.be.empty;
        done()
      })
      .catch(err => done(err));
  });

  it("create relationship between User and Skill", done => {
    request.post("/relationship")
      .set("Authorization", `JWT ${adminToken}`)
      .send({from: adminUserId, to: skillId})
      .expect(200)
      .then((res) => {
        expect(res.body.id).a("string")
        expect(res.body.start).to.eql(adminUserId)
        expect(res.body.end).to.eql(skillId)
        expect(res.body.type).to.eql("HAS_ROLE")
        expect(res.body.properties).to.be.empty;
        done()
      })
      .catch(err => done(err));
  });


});