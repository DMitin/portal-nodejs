const downloadUtils = require('../util/downloadFiles');
const neo4jInit = require('../util/initNeo4j');
const crud = require('../util/crud')();

describe("Uploading user image", () => {

  var fileName;
  var adminUserId, adminToken;

  before(done => {
    neo4jInit.initNeo4jPromise()
      .then(() => crud.createAdminUserCredentials())
      .then(({userId, token}) => {
        adminUserId = userId;
        adminToken = token;
      })
      .then(() => done())
      .catch(err => done(err));
  });

  it("can upload user image", done => {
    request.post("/upload")
      .attach('avatar', downloadUtils.getCommonTestFilePath())
      .set("Authorization", `JWT ${adminToken}`)
      .expect(200)
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          fileName = res.text;
          done();
        }
      });
  });
  it("can download, previously uploaded user image", done => {
    request.get('/images/' + fileName)
      .set("Authorization", `JWT ${adminToken}`)
      .expect(200)
      .end((err, res) => {
        done(err);
      });
  });

});