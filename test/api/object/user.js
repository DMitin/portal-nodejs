const neo4jInit = require('../../util/initNeo4j');
const download = require('../../util/downloadFiles');
const crud = require('../../util/crud')();
const auth = require('../../util/auth');

const foxFile = "fox.png";

describe("Users API", () => {

  var adminUser;
  var adminRole;
  var token;

  before(done => {
    neo4jInit.initNeo4jPromise()
      .then(() => download.removeDownloadedFile(foxFile))
      .then(() => crud.createCommonUser())
      .then((id) => {adminUser = id; token = auth.getToken(adminUser);})
      .then(() => crud.createCommonRole("Admin"))
      .then((id) => adminRole = id)
      .then(() => crud.createRelationship(adminUser, adminRole, "HAS_ROLE"))
      .then(() => done())
      .catch(err => done(err));
  });

    it("can get all users", done => {
        request.get("/users")
        .set("Authorization", `JWT ${token}`)
        .end((err, res) => {
            expect(res.body[0].id).a("string");
            expect(res.body[0].properties).to.eql(crud.commonUserData);
            done(err)
        });
    })

  it("can't create user without full data (firstName, lastName, image)", done => {

    const data = {firstName: "Denis", lastName: "Mitin"};
    request.post("/users")
      .set("Authorization", `JWT ${token}`)
      .send(data)
      .expect(406)
      .end((err) => { //unused second parametr res
        done(err)
      });
  });

  it("can't create user without previously uploading image", done => {

    const data = {
      firstName: "Denis",
      lastName: "Mitin",
      image: "fox.png" // file was removed by before handler
    };
    request.post("/users")
      .set("Authorization", `JWT ${token}`)
      .send(data)
      .expect(406)
      .end((err) => { //unused second parametr res
        done(err)
      });
  });

  it("create user and test response data", done => {

    const data = crud.commonUserData;
    download.copyTestFoxFileToUploaded(foxFile)
      .then(() => {
        request.post("/users")
          .set("Authorization", `JWT ${token}`)
          .send(data)
          .expect(200)
          .end((err, res) => {
            if (err) done(err);
            expect(res.body.id).a("string");
            expect(res.body.properties).to.eql(data);
            done();
          });

      }).catch(err => done(err));
  });
/*
    it("can get all users", done => {
        request.get("/users")
        .set("Authorization", `JWT ${token}`)
        .end((err, res) => {
            console.log(res.body)
            expect(res.body.id).a("string");
            expect(res.body[0].properties).to.eql(data);
            done(err)
        });
    });
    */
});