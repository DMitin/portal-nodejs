const neo4jInit = require('../../util/initNeo4j');
const download = require('../../util/downloadFiles');
const crud = require('../../util/crud')();
const auth = require('../../util/auth');


const neo4jObject = require('../../../src/business/neo4j-object')();

var adminUser;
var adminRole;
var token;

const skillName = "Java";

before(done => {
  neo4jInit.initNeo4jPromise()
    .then(() => crud.createCommonUser())
    .then((id) => {adminUser = id; token = auth.getToken(adminUser);})
    .then(() => crud.createCommonRole("Admin"))
    .then((id) => adminRole = id)
    .then(() => crud.createRelationship(adminUser, adminRole, "HAS_ROLE"))
    .then(() => neo4jObject.createObject("Skill", {
      name: skillName
    }))
    .then(() => done())
    .catch(err => done(err));
});

it("can get all skills", done => {
  request.get("/skills")
    .set("Authorization", `JWT ${token}`)
    .end((err, res) => {

      expect(res.body[0].id).a("string");
      expect(res.body[0].properties.name).to.eql(skillName);
      done(err)
    });
})