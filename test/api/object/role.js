const neo4jInit = require('../../util/initNeo4j');
const crud = require('../../util/crud')();

describe("Roles API", () => {

  var adminUserId, adminToken;

  before(done => {
    neo4jInit.initNeo4jPromise()
      .then(() => crud.createAdminUserCredentials())
      .then(({userId, token}) => {
        adminUserId = userId;
        adminToken = token;
      })
      .then(() => done())
      .catch(err => done(err));
  });


  it("create role without name", done => {

    const data = {name: "HR"};
    request.post("/roles")
      .set("Authorization", `JWT ${adminToken}`)
      .expect(406)
      .end((err, res) => {
        done(err);
      });
  });

  it("create role and test response data", done => {

    const data = {name: "HR"};
    request.post("/roles")
      .set("Authorization", `JWT ${adminToken}`)
      .send(data)
      .expect(200)
      .end((err, res) => {
        if (err) done(err);
        expect(res.body.id).a("string");
        expect(res.body.properties).to.eql(data);
        done();
      });
  });
});