const neo4jInit = require('../../util/initNeo4j');
const crud = require('../../util/crud')();

describe("Objects that fetched with dependencies", () => {

  var connectedUser, disconnectedUser;
  var role1, role2;
  var justToken;

  before(done => {
    neo4jInit.initNeo4jPromise()
      .then(() => crud.createCommonUserCredentials())
      .then(({userId, token}) => {
        connectedUser = userId;
        justToken = token;
      })
      .then(() => crud.createCommonUser())
      .then((id) => disconnectedUser = id)
      .then(() => crud.createCommonRole("RoleTest1"))
      .then((id) => role1 = id)
      .then(() => crud.createCommonRole("RoleTest2"))
      .then((id) => role2 = id)
      .then(() => crud.createRelationship(connectedUser, role1, "HAS_ROLE1"))
      .then(() => crud.createRelationship(connectedUser, role2, "HAS_ROLE2"))
      .then(() => done())
      .catch(err => done(err));
  });

  it("get disconnectedUser (with no relations)", done => {
    request.get(`/users/${disconnectedUser}`)
      .set("Authorization", `JWT ${justToken}`)
      .expect(200)
      .end((err, res) => {
        if (err) done(err);
        expect(res.body.properties).to.eql(crud.commonUserData);
        expect(res.body.relations).to.be.empty;
        done()
      });
  });

  it("get connectedUser (with relations)", done => {
    request.get(`/users/${connectedUser}`)
      .set("Authorization", `JWT ${justToken}`)
      .expect(200)
      .end((err, res) => {
        if (err) done(err);
        expect(res.body.properties).to.eql(crud.commonUserData);
        expect(res.body.relations.length).to.be.eql(2);
        expect(res.body.relations.map(r => r.relInfo.type))
          .to.be.an('array')
          .that.includes('HAS_ROLE1', 'HAS_ROLE2');

        expect(res.body.relations.map(r => r.node.properties.name))
          .to.be.an('array')
          .that.includes('RoleTest1', 'RoleTest2');
        done()
      });
  })
});