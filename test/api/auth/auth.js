const neo4jInit = require('../../util/initNeo4j');
const crud = require('../../util/crud')();
const cfg = require('../../../config/auth.config');
const jwt = require("jwt-simple");
const {validUsers, notValidUser} = require("../../../src/ldap/ldap.test");


describe("Auth API", () => {
  before(done => {
    neo4jInit.initNeo4jPromise()
      .then(() => crud.createCommonUser())
      .then(() => done())
      .catch(err => done(err));
  });

  it("get token for existing ldapName", done => {
    request.post("/token")
      .send(validUsers[0])
      .expect(200)
      .end((err, res) => {
        if (err) done(err)
        const token = res.body.token;
        expect(token).a("string");
        const decoded = jwt.decode(token, cfg.jwtSecret);
        expect(decoded)
          .to.have.property("id")
          .a("string");
        done()
      });
  });

  it("get 401 when provide wrong credentials", done => {
    request.post("/token")
      .send({zzz: "zzz"}) // wrong credential
      .expect(401)
      .end(err => done(err));
  });

  it("get 401 when not valid credentials", done => {
    request.post("/token")
      .send(notValidUser)
      .expect(401)
      .end(err => done(err));
  });
});