const neo4jInit = require('../../../util/initNeo4j');
const crud = require('../../../util/crud')();

describe("Grants for Role API", () => {

  var notAdminToken;

  before(done => {
    neo4jInit.initNeo4jPromise()
      .then(() => crud.createCommonUserCredentials())
      .then(({userId, token}) => {
        notAdminToken = token;
      })
      .then(() => done())
      .catch(err => done(err));
  });


  it("Only Admins can create new roles", done => {

    const data = {name: "HR"};
    request.post("/roles")
      .set("Authorization", `JWT ${notAdminToken}`)
      .send(data)
      .expect(401)
      .then(() => done())
      .catch(err => done(err));
  });
});