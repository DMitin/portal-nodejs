const neo4jInit = require('../../../util/initNeo4j');
const crud = require('../../../util/crud')();

describe("Grants for Creating relation between User and role", () => {

  let notAdminUser;
  let notAdminToken;
  let roleId;

  before(done => {
    neo4jInit.initNeo4jPromise()
      .then(() => crud.createCommonUserCredentials())
      .then(({userId, token}) => {
        notAdminUser = userId;
        notAdminToken = token;
      })
      .then(() => crud.createCommonRole())
      .then((id) => roleId = id)
      .then(() => done())
      .catch(err => done(err));
  });


  it("Only Admins can add role to user", done => {
    request.post("/rel/addRoleToUser")
      .set("Authorization", `JWT ${notAdminToken}`)
      .send({from: notAdminUser, to: roleId})
      .expect(401)
      .then(() => done())
      .catch(err => done(err));
  });

});