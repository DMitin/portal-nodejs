const neo4jInit = require('../../../util/initNeo4j');
const download = require('../../../util/downloadFiles');
const crud = require('../../../util/crud')();


const foxFile = "fox.png";

describe("Grants for Users API", () => {

  var notAdminUser, likeAdminUser;
  var notAdminRole, likeAdminRole;
  var notAdminToken, likeAdminToken;

  before(done => {
    neo4jInit.initNeo4jPromise()
      .then(() => crud.createCommonUserCredentials())
      .then(({userId, token}) => {
        notAdminUser = userId;
        notAdminToken = token;
      })
      .then(() => crud.createCommonUserCredentials())
      .then(({userId, token}) => {
        likeAdminUser = userId;
        likeAdminToken = token;
      })
      .then(() => crud.createCommonRole("Not_Admin"))
      .then((id) => notAdminRole = id)
      .then(() => crud.createCommonRole("Admin1"))
      .then((id) => likeAdminRole = id)
      .then(() => crud.createRelationship(notAdminUser, notAdminRole, "HAS_ROLE"))
      .then(() => crud.createRelationship(likeAdminUser, likeAdminRole, "HAS_ROLE"))
      .then(() => done())
      .catch(err => done(err));
  });


  it("Only Admins can create new users", done => {

    const data = crud.commonUserData;
    download.copyTestFoxFileToUploaded(foxFile)
      .then(() => {
        request.post("/users")
          .set("Authorization", `JWT ${notAdminToken}`)
          .send(data)
          .expect(401)
          .end((err, res) => {
            done(err);
          });

      }).catch(err => done(err));
  });

  it("User that have role like admin can't execute", done => {

    const data = crud.commonUserData;
    request.post("/users")
      .set("Authorization", `JWT ${likeAdminToken}`)
      .send(data)
      .expect(401)
      .end((err, res) => {
        done(err);
      });


  });
});