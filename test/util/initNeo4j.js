const session = require('../../src/business/neo4j');

module.exports = {
  initNeo4j: done => {
    session.run(
      'MATCH (n) DETACH DELETE n'
    ).then(result => {
      session.close();
      done();
    }).catch(error => {
      session.close();
      done(error);
    });
  },
  initNeo4jPromise: () => {
    return session.run(
      'MATCH (n) DETACH DELETE n'
    ).then(result => {
      session.close();
    }).catch(error => {
      session.close();
    });
  }
}