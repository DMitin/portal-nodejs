var jwt = require("jwt-simple");
const cfg = require('../../config/auth.config');

module.exports = {
  getToken: (id) => {
    return jwt.encode({
      id
    }, cfg.jwtSecret);
  }
};