const neo4jObject = require('../../src/business/neo4j-object')();
const neo4jRelationship = require('../../src/business/neo4j-relationship')();
const auth = require('./auth');
const routeUtil = require('../../src/routes/util');


const commonUserData = {
  firstName: "Denis",
  lastName: "Mitin",
  image: "fox.png",
  ldapName: "dMitin"
};

const commonUserIvanov = {
    firstName: "Ivan",
    lastName: "Ivanov",
    image: "fox.png",
    ldapName: "iIvanov"
};

module.exports = () => {

  const createCommonUser = (userData = commonUserData) => {
    const preparedData = routeUtil.addSearchStringToUserData(userData);
    return neo4jObject.createObject("User", preparedData)
      .then(res => res.id);
  };

  const createCommonUserCredentials = (userData = commonUserData) => {
    return createCommonUser(userData)
      .then((userId) => {
        return {
          userId,
          token: auth.getToken(userId)
        }
      });
  };

  const createCommonRole = (name = "RoleTest") => {
    return neo4jObject.createObject("Role", {name})
      .then(res => res.id);
  };

  const createRelationship = (idFrom, idTo, relType) => {
    return neo4jRelationship
      .createRelationship(idFrom, idTo, relType)
      .then(result => result.records[0]._fields[0]);
  };

  const createAdminUserCredentials = (userData = commonUserData) => {
    var userId;
    return createCommonUser()
      .then(id => userId = id)
      .then(() => createCommonRole("Admin")) //TODO вынести в neo4j-common
      .then((roleId) => createRelationship(userId, roleId, "HAS_ROLE")) //TODO вынести в neo4j-common
      .then((relId) => {
        return {
          userId,
          token: auth.getToken(userId)
        }
      });
  };
  return {
    commonUserData,
    commonUserIvanov,
    createCommonUser,
    createCommonUserCredentials,
    createCommonRole,
    createRelationship,
    createAdminUserCredentials
  }

};