const fs = require('fs-extra');
const path = require('../../config/path');

const foxFilePath = "test/files/fox.png";

module.exports = {

  getCommonTestFilePath: () => foxFilePath,
  removeDownloadedFile: (fileName) => {
    const filePath = path.imagesFolderPath + "/" + fileName;
    return fs
      .unlink(filePath)
      .catch(err => {
      })
  },
  copyTestFoxFileToUploaded: (filename) => fs.copy(foxFilePath, path.imagesFolderPath + "/" + filename)
};